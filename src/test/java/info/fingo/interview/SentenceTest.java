package info.fingo.interview;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class SentenceTest {

    @Test
    void getSentenceOK() {
        assertEquals(new Sentence().getSentence(), "Hello!");
    }

    @Test
    void getSentenceIncorrect() {
        assertNotEquals (new Sentence().getSentence(), "Hello!");
    }
}